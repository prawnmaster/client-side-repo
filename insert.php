<?php

/**
 * This file contains PHP functions to insert code into a MySQL database
 * Written by Nasra Terzi
 * Modifications Samuel Clements
 * Date: 21/09/2015
 */



/**
 * Function to insert a tuple into the ft_view_tabs table.
 * @return informs if the query was successful or not
 */
function ft_view_tabs($view_id, $tab_number, $tab_label) 
{
	global $g_table_prefix;

	$insert_query = "INSERT INTO {$g_table_prefix}view_tabs (view_id, tab_number, tab_label)
		VALUES ($view_id, $tab_number, $tab_label
			)";
	$result = mysqli_query($insert_query);
	return $result;
}

/**
 * Function to insert a tuple into the ft_views table.
 * @return informs if the query was successful or not
 */
function ft_views($view_id, $form_id, $access_type, $view_name, $view_order, $is_new_sort_group, $group_id, $num_submissions_per_page, $default_sort_field, $default_sort_filed_order, $may_add_submissions, $may_edit_submissions, $may_delete_submissions, $has_client_map_filter, $has_standard_filter) 
{
	global $g_table_prefix;

	$insert_query = "INSERT INTO {$g_table_prefix}views (view_id, form_id, access_type, view_name, view_order, is_new_sort_group, group_id,
 			num_submissions_per_page, default_sort_field, default_sort_filed_order, may_add_submissions, 
			may_edit_submissions, may_delete_submissions, has_client_map_filter, has_standard_filter)
		VALUES ($view_id, $form_id, $access_type, $view_name, $view_order, $is_new_sort_group, $group_id,
 			$num_submissions_per_page, $default_sort_field, $default_sort_filed_order, $may_add_submissions, 
			$may_edit_submissions, $may_delete_submissions, $has_client_map_filter, $has_standard_filter
			)";
	$result = mysqli_query($insert_query);
	return $result;
}

/**
 * Function to insert a tuple into the ft_forms table.
 * @return informs if the query was successful or not
 */
function ft_forms($form_id, $form_type, $access_type, $submission_type, $date_created, $is_active, $is_initialized, $is_complete, $is_multi_page_form, $form_name, $form_url, $redirect_url, $auto_delete_submission_files, $submission_strip_tags, $edit_submission_page_label, $add_submission_button_label) 
{
	global $g_table_prefix;

	$insert_query = "INSERT INTO {$g_table_prefix}forms (form_id, form_type, access_type, submission_type, date_created, is_active, is_initialized, 
			is_complete, is_multi_page_form, form_name, form_url, redirect_url, auto_delete_submission_files, 
			submission_strip_tags, edit_submission_page_label, add_submission_button_label)
		VALUES ($form_id, $form_type, $access_type, $submission_type, $date_created, $is_active, $is_initialized, 
			$is_complete, $is_multi_page_form, $form_name, $form_url, $redirect_url, $auto_delete_submission_files, 
			$submission_strip_tags, $edit_submission_page_label, $add_submission_button_label
			)";
	$result = mysqli_query($insert_query);
	return $result;
}

/**
 * Function to insert a tuple into the ft_module_form_builder_forms table.
 * @return informs if the query was successful or not
 */
function ft_module_form_builder_forms($published_form_id, $is_online, $is_published, $form_id, $view_id, $set_id, $publish_date, $filename, $folder_path, $folder_url, $include_review_page, $include_thanks_page_in_nav, $thankyou_page_content, $form_offline_page_content, $review_page_title, $thankyou_page_title, $offline_date, $list_order) 
{
	global $g_table_prefix;

	$insert_query = "INSERT INTO {$g_table_prefix}module_form_builder_forms (published_form_id, is_online, is_published, form_id, view_id, set_id, 
			publish_date, filename, folder_path, folder_url, include_review_page, include_thanks_page_in_nav, thankyou_page_content, 
			form_offline_page_content, review_page_title, thankyou_page_title, offline_date, list_order)
		VALUES ($published_form_id, $is_online, $is_published, $form_id, $view_id, $set_id, $publish_date, $filename, 
			$folder_path, $folder_url, $include_review_page, $include_thanks_page_in_nav, $thankyou_page_content, 
			$form_offline_page_content, $review_page_title, $thankyou_page_title, $offline_date, $list_order
			)";
	$result = mysqli_query($insert_query);
	return $result;
}

/**
 * Function to insert a tuple into the ft_module_form_builder_form_templates table.
 * @return informs if the query was successful or not
 */
function ft_module_form_builder_form_templates($published_form_id, $template_type, $template_id) 
{
	global $g_table_prefix;

	$insert_query = "INSERT INTO {$g_table_prefix}module_form_builder_form_templates (published_form_id, template_type, template_id) 
		VALUES ($published_form_id, $template_type, $template_id
			)";
	$result = mysqli_query($insert_query);
	return $result;
}

/**
 * Function to insert a tuple into the ft_module_form_builder_form_placeholders table.
 * @return informs if the query was successful or not
 */
function ft_module_form_builder_form_placeholders($published_form_id, $placeholder_id, $placeholder_value) 
{
	global $g_table_prefix;

	$insert_query = "INSERT INTO {$g_table_prefix}module_form_builder_form_placeholders (published_form_id, placeholder_id, placeholder_value)
		VALUES ($published_form_id, $placeholder_id, $placeholder_value
			)";
	$result = mysqli_query($insert_query);
	return $result;
}

/**
 * Function to insert a tuple into the ft_form_fields table.
 * @return informs if the query was successful or not
 */
function ft_form_fields($field_id, $form_id, $field_name, $field_test_value, $field_size, $field_type_id, $is_system_field, $data_type, $field_title, $col_name, $list_order, $is_new_sort_group, $include_on_redirect) 
{
	global $g_table_prefix;

	$insert_query = "INSERT INTO {$g_table_prefix}form_fields (field_id, form_id, field_name, field_test_value, field_size, field_type_id, 
			is_system_field, data_type, field_title, col_name, list_order, is_new_sort_group, include_on_redirect)
		VALUES ($field_id, $form_id, $field_name, $field_test_value, $field_size, $field_type_id, $is_system_field, $data_type, 
			$field_title, $col_name, $list_order, $is_new_sort_group, $include_on_redirect
			)";
	$result = mysqli_query($insert_query);
	return $result;
}

/**
 * Function to insert a tuple into the ft_view_fields table.
 * @return informs if the query was successful or not
 */
function ft_view_fields($view_id, $field_id, $group_id, $is_editable, $is_searchable, $list_order, $is_new_sort_group) 
{
	global $g_table_prefix;

	$insert_query = "INSERT INTO {$g_table_prefix}view_fields (view_id, field_id, group_id, is_editable, is_searchable, list_order, is_new_sort_group)
		VALUES ($view_id, $field_id, $group_id, $is_editable, $is_searchable, $list_order, $is_new_sort_group
			)";
	$result = mysqli_query($insert_query);
	return $result;
}

/**
 * Function to insert a tuple into the ft_module_form_builder_templates table.
 * Note: not sure if this will be required. Keep in though
 * 
 * @return informs if the query was successful or not
 */
function ft_module_form_builder_templates($template_id, $set_id, $template_type, $template_name, $content, $list_order) 
{
	global $g_table_prefix;

	$insert_query = "INSERT INTO {$g_table_prefix}module_form_builder_templates (template_id, set_id, template_type, template_name, content, list_order)
		VALUES ($template_id, $set_id, $template_type, $template_name, $content, $list_order
			)";
	$result = mysqli_query($insert_query);
	return $result;
}

/**
 * Function to insert a tuple into the ft_view_columns table.
 * @return informs if the query was successful or not
 */
function ft_view_columns($view_id, $field_id, $list_order, $is_sortable, $auto_size, $custom_width, $truncate) 
{
	global $g_table_prefix;

	$insert_query = "INSERT INTO {$g_table_prefix}view_columns (view_id, field_id, list_order, is_sortable, auto_size, custom_width, truncate)
		VALUES ($view_id, $field_id, $list_order, $is_sortable, $auto_size, $custom_width, $truncate
			)";
	$result = mysqli_query($insert_query);
	return $result;
}

/**
 * Function to insert a tuple into the ft_field_settings table.
 * @return informs if the query was successful or not
 */
function ft_field_settings($field_id, $setting_id, $setting_value)
{
	global $g_table_prefix;

	$insert_query = "INSERT INTO {$g_table_prefix}field_settings (field_id, setting_id, setting_value)
		VALUES ($field_id, $setting_id, $setting_value
			)";
	$result = mysqli_query($insert_query);
	return $result;
}

/**
 * Function to insert a tuple into the ft_list_groups table.
 * @return informs if the query was successful or not
 */
function ft_list_groups($group_id, $group_type, $group_name, $custom_data, $list_order) 
{
	global $g_table_prefix;

	$insert_query = "INSERT INTO {$g_table_prefix}list_groups (group_id, group_type, group_name, custom_data, list_order)  
		VALUES ($group_id, $group_type, $group_name, $custom_data, $list_order
			)";
	$result = mysqli_query($insert_query);
	return $result;
}

/**
 * Function to insert a tuple into the ft_option_lists table.
 * @return informs if the query was successful or not
 */
function ft_option_lists($list_id, $option_list_name, $is_grouped, $original_form_id) 
{
	global $g_table_prefix;

	$insert_query = "INSERT INTO {$g_table_prefix}option_lists (list_id, option_list_name, is_grouped, original_form_id)
		VALUES ($list_id, $option_list_name, $is_grouped, $original_form_id
			)";
	$result = mysqli_query($insert_query);
	return $result;
}

/**
 * Function to insert a tuple into the ft_field_options table.
 * @return informs if the query was successful or not
 */
function ft_field_options($list_id, $list_group_id, $option_order, $option_value, $option_name, $is_new_sort_group) 
{
	global $g_table_prefix;

	$insert_query = "INSERT INTO {$g_table_prefix}field_options (list_id, list_group_id, option_order, option_value, option_name, is_new_sort_group) 
		VALUES ($list_id, $list_group_id, $option_order, $option_value, $option_name, $is_new_sort_group
			)";
	$result = mysqli_query($insert_query);
	return $result;
}



//not required:

/*function ft_form_1($submission_id, $col_1, $submission_date, $last_modified_date, $ip_address, $is_finalized, $col_2) {
	$insert_query = "INSERT INTO {$g_table_prefix}form_1 () 
		VALUES ($submission_id, $col_1, $submission_date, $last_modified_date, $ip_address, $is_finalized, $col_2)";
	$result = mysqli_query($insert_query);
	return $result;
}*/




?>