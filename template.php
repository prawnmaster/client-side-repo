<!DOCTYPE html>
<html lang="en">
<head>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</head>
<body>
	<nav class="navbar navbar-inverse">
		<div class="container-fluid">
			<div class="navbar-header">
				<a href="#" class="navbar-brand"></a>
			</div>

			<div>
				<ul class="nav navbar-nav">
					<li class="active"><a href="#">Create</a></li>
					<li><a href="#">Preview</a></li>
					<li><a href="#">Analyitics</a></li>
				</ul>
			</div>
		</div>
	</nav>
	<div class="row">
		<div class="col-md-3"></div>
			<div class="col-md-6">
				<div class="nav nav-pills" role="tablist">
  					<span class="badge">1</span>
				</div>
			</div>
	</div>

	<p></p>
	<div class="row">
		<div class="col-md-3"></div>
			<div class = "col-md-6">
				<div class="input-group">
					<span class="input-group-addon" id="basic-addon1">Question:</span>
					<form action="template.php" method="post">
					<input type="text" class="form-control" aria-describedby="basic-addon1" name="question">
					<form/>
				</div>
			</div>
		<div class="col-md-3"></div>
	</div>
	<p></p>
	<div class="row">
		<div class="col-md-3"></div>
		<div class = "col-md-6">
			<div class="input-group">
				<span class="input-group-addon" id="basic-addon1">Youtube Link:</span>
				<form action="template.php" method="post">
				<input type="text" class="form-control" aria-describedby="basic-addon1" name="link">
				<form/>
			</div>
		</div>
		<div class="col-md-3"></div>
	</div>
	<p></p>
	<div class="row">
		<div class="col-md-3"></div>
		<div class = "col-md-6">
			<div class="input-group">
				<span class="input-group-addon" id="basic-addon1">Correct Answer:</span>
				<form action="template.php" method="post">
				<input type="text" class="form-control" aria-describedby="basic-addon1" name="correct">
				<form/>
			</div>
		</div>
		<div class="col-md-3"></div>
	</div>
	<p></p>
	<div class="row">
		<div class="col-md-3"></div>
		<div class = "col-md-6">
			<div class="input-group">
				<span class="input-group-addon" id="basic-addon1">Alternate Answer:</span>
				<form action="template.php" method="post">
				<input type="text" class="form-control" aria-describedby="basic-addon1" name="alternate1">
				<form/>
			</div>
		</div>
		<div class="col-md-3"></div>
	</div>
	<p></p>
	<div class="row">
		<div class="col-md-3"></div>
		<div class = "col-md-6">
			<div class="input-group">
				<span class="input-group-addon" id="basic-addon1">Alternate Answer:</span>
				<form action="template.php" method="post">
				<input type="text" class="form-control" aria-describedby="basic-addon1" name="alternate2">
				<form/>
			</div>
		</div>
		<div class="col-md-3"></div>
	</div>
	<p></p>
	<div class="row">
		<div class="col-md-3"></div>
			<div class="col-md-6">
				<div class="input-group">
					<span class="input-group-addon" id="basic-addon1">Alternate Answer:</span>
					<form action="template.php" method="post">
					<input type="text" class="form-control" aria-describedby="basic-addon1" name="alternate3">
					<form/>
				</div>
			</div>
	</div>
	<p></p>
	<div class="row">
		<div class="col-md-3"></div>
			<div class="col-md-6">
				<div class="nav nav-pills" role="tablist">
  					<span class="badge">2</span>
				</div>
			</div>
	</div>
	<div class = "row">
		<div class="col-sm-6"></div>
				<div class="btn-group" role="group" aria-label="...">
 					<button type="button" class="btn btn-default">Create</button>
					<input type="submit" name="submit" value="Submit" />
				</div>
			</div>
	</div>
</body>
</html>
Lockie Drysdale
<p></p>

<?php

if (isset($_POST["question"])) {
	if ($_POST["question"] != NULL) {
		echo($_POST['question'] . "<br>");
	} else echo "Please enter a question <br>";
}

if (isset($_POST["link"])) {
	if ($_POST["link"] != NULL) {
		echo($_POST['link'] . "<br>");
	} else echo "Please enter a youtube video link <br>";
}

if (isset($_POST["correct"])) {
	if ($_POST["correct"] != NULL) {
		echo($_POST['correct'] . "<br>");
	} else echo "Please enter the correct question <br>";
}

if (isset($_POST["alternate1"])) {
	if ($_POST["alternate1"] != NULL) {
		echo($_POST['alternate1'] . "<br>");
	} else echo "Please enter the first alternate answer <br>";
}

if (isset($_POST["alternate2"])) {
	if ($_POST["alternate2"] != NULL) {
		echo($_POST['alternate2'] . "<br>");
	} else echo "Please enter the second alternate answer <br>";
}

if (isset($_POST["alternate3"])) {
	if ($_POST["alternate3"] != NULL) {
		echo($_POST['alternate3'] . "<br>");
	} else echo "Please enter the third alternate answer <br>";
}

?>